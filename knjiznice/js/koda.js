
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var dataZaWHO;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

var bolniki = [];

var soGeneriraniPodatki = false;
function generirajOseba() {
  if(!soGeneriraniPodatki) {
    generirajPodatke(1);
    soGeneriraniPodatki = true;
  }
}

function generirajPodatke(stPacienta) {
  
  var ehrId = "";
  
  var ime;
	var priimek;
  var datumRojstva;
  var trenutnaTelesnaTeza;
  var trenutnaTelesnaVisina;
  var zazeljenaTelesnaTeza;
  var spol;
  var drzava;
  
  if(stPacienta == 1) {
   ime = "Boba";
  	priimek = "de Borgo";
    datumRojstva = "1966-04-04";
    trenutnaTelesnaTeza = "85";
    trenutnaTelesnaVisina = "1.8";
    zazeljenaTelesnaTeza = "80";
    spol = "MALE";
    drzava ="Italy";
  } else if(stPacienta == 2) {
    ime = "AntoniA";
  	priimek = "Su";
    datumRojstva = "1966-04-04";
    trenutnaTelesnaTeza = "110";
    trenutnaTelesnaVisina = "2";
    zazeljenaTelesnaTeza = "90";
    spol = "FEMALE";
    drzava ="Australia";
  } else if(stPacienta == 3) {
    ime = "Igor";
  	priimek = "Bwgi";
    datumRojstva = "1999-05-24";
    trenutnaTelesnaTeza = "100";
    trenutnaTelesnaVisina = "1.75";
    zazeljenaTelesnaTeza = "70";
    spol = "MALE";
    drzava ="France";
  } else {
    return;
  }
  
  if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          gender: spol,
          additionalInfo: {"ehrId": ehrId, "trenutnaTelesnaTeza": trenutnaTelesnaTeza, "trenutnaTelesnaVisina": trenutnaTelesnaVisina, "zazeljenaTelesnaTeza":zazeljenaTelesnaTeza, "drzava": drzava}
        };
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              
                bolniki[stPacienta - 1] = partyData;
                dodajOpcijo(partyData);
                prikaziNapako("Generiran pacient številka: " + stPacienta + " s EHRid: " + ehrId);
                generirajPodatke(stPacienta + 1);
            }
          },
          error: function(err) {
          	prikaziNapako("Napaka " + JSON.parse(err.responseText).userMessage + "!");
          }
        });
      },
      error: function(err) {
          	prikaziNapako("Napaka " + JSON.parse(err.responseText).userMessage + "!");
      }
		});
	}
	
  return ehrId;
}

function prikaziNapako(message) {
    window.alert(message);
}

function dodajOpcijo(data) {
    var select = document.getElementById("preberiObstojeciEHR");
    var option = document.createElement("option");
    option.text = data.firstNames + " " + data.lastNames;
    option.value = data.additionalInfo.ehrId;
    select.add(option);
}

function vrniBolnik(EHRid) {
  for(var i = 0; i < 3; i++) {
    if(bolniki[i].additionalInfo.ehrId == EHRid) {
      return bolniki[i];
    }
  }
}

function preberiEHRzaBolnika() {
    
    var trenutenEHRid = document.getElementById("preberiEHRid").value;
    if(trenutenEHRid == "" || trenutenEHRid == null) {
    prikaziNapako("Prosim selekcionirate bolnika ali pa generirate bolnike :)");
    return;
  }
    
    for(var i = 0; i < 3; i++) {
        var bolnik = bolniki[i];
        
        if(bolnik.additionalInfo.ehrId == trenutenEHRid) {
            
            var fullName = document.getElementById("patient-name");
            var age = document.getElementById("datumRojstvaBolnikaField");
            var gender = document.getElementById("patient-gender");
            var drzavaField = document.getElementById("patient-country");
            var trenutnaTezaField = document.getElementById("trenutnaTezaField");
            var trenutnaVisinaField = document.getElementById("trenutnaVisinaField");
            var trenutniBMIField = document.getElementById("trenutniBMIField");
            var trenutniBMIStatusField = document.getElementById("trenutniBMIStatusField");
            var zazeljenaTezaField = document.getElementById("zazeljenaTezaField");
            var zazeljenaBMIField = document.getElementById("zazeljeniBMIField");
            
            
            fullName.innerHTML = bolnik.firstNames + " " + bolnik.lastNames;
            gender.innerHTML = bolnik.gender;
            age.innerHTML = bolnik.dateOfBirth;
            drzavaField.innerHTML = bolnik.additionalInfo.drzava;
            
            trenutnaTezaField.innerHTML = bolnik.additionalInfo.trenutnaTelesnaTeza;
            trenutnaVisinaField.innerHTML = bolnik.additionalInfo.trenutnaTelesnaVisina;
            trenutniBMIField.innerHTML = bolnik.additionalInfo.trenutnaTelesnaTeza/(bolnik.additionalInfo.trenutnaTelesnaVisina * bolnik.additionalInfo.trenutnaTelesnaVisina);
            
            zazeljenaTezaField.innerHTML = bolnik.additionalInfo.zazeljenaTelesnaTeza;
            zazeljenaBMIField.innerHTML = bolnik.additionalInfo.zazeljenaTelesnaTeza/(bolnik.additionalInfo.trenutnaTelesnaVisina * bolnik.additionalInfo.trenutnaTelesnaVisina);
            
            prikaziPodatkeUrbanHealth();
            
            break;
        }
    }
    
}


function izracunDataZaGraf(molt) {
  
  var trenutenEHRid = document.getElementById("preberiEHRid").value;
  if(trenutenEHRid == "" || trenutenEHRid == null) {
    prikaziNapako("Prosim selekcionirate bolnika ali pa generirate bolnike :)");
    return;
  }
  
    var steviloKgIzgube = document.getElementById("trenutnaTezaField").innerHTML - document.getElementById("zazeljenaTezaField").innerHTML;
    var stTednov = steviloKgIzgube/molt + 1;
    var data = [];
    var koncnaTeza = 0;
    for(var i = 0; i < stTednov; i++) {
        var y = document.getElementById("trenutnaTezaField").innerHTML - molt * i;
        var name = "Bolnik v tednu " + i + " ima težo: ";
        var item = {
            x: i,
            y: y,
            name: name
        };
        koncnaTeza = item.y;
        data[i] = item;
    }
    hiddenKomentar(false);
    spremeniKomentar(stTednov, koncnaTeza, molt);
    prikaziGraf(data, 0, "Kako bo bolnim izgubil težo v tednih?");
}

function pokaziHiterGraf() {
    izracunDataZaGraf(2);
}

function prikaziSrednjiGraf() {
    izracunDataZaGraf(1);
}

function prikaziPocasenGraf() {
    izracunDataZaGraf(0.5);
}

function prikaziGraf(data, xMin, title) {
    var chart = new CanvasJS.Chart("myChart", {
    	animationEnabled: true,
    	theme: "light2",
    	title:{
    		text: title,
    	},
    	axisY:{
    		includeZero: false
    	},
    	axisX:{
           minimum: xMin,
         },
    	data: [{        
    		type: "line",       
    		dataPoints: data
    	}]
    });
    chart.render();
}

function spremeniKomentar(stTednov, koncnaTeza, kgNaTeden) {
  var komentar = document.getElementById("velocityKomentar");
  var ime = document.getElementById("patient-name").innerHTML;
  
  komentar.innerHTML = ime + " mora telovadit " + stTednov + " tednov, da bo dosegel tezo " + koncnaTeza + "kg. Predvidoma bo zgubil " + kgNaTeden + " kg na teden.";
}


function staorost(birthday) {
  
  var year = 0;
  year += birthday.charAt(0);
  year += birthday.charAt(1);
  year += birthday.charAt(2);
  year += birthday.charAt(3);
  var today = new Date();
  
  return today.getFullYear() - year;
}

function hiddenKomentar(isHidden) {
  if(isHidden) {
    document.getElementById("velocityKomentar").style.display= 'none';
  } else {
    document.getElementById("velocityKomentar").style.display= '';
  }
}

function prikaziPodatkeUrbanHealth() {
  
  var trenutenEHRid = document.getElementById("preberiEHRid").value;
  var bolnik = vrniBolnik(trenutenEHRid);
  
  
  console.log("Deluje2");
  
  $.getJSON('/knjiznice/json/data.json', function(data) {
   var facts = data.fact;
   var data = [];
   var numberOfData = 0;
   
   for(var i = 0; i < facts.length; i++) {
     if(facts[i].dims.COUNTRY == bolnik.additionalInfo.drzava) {
       var x = parseInt(facts[i].dims.YEAR, 10);
       var y = parseInt(facts[i].Value, 10);
       var item = {
         x: x,
         y: y
       }
       data[numberOfData] = item;
       numberOfData += 1;
     }
   }
   var novaData = [];
   for(var i = 0; i < numberOfData; i++) {
     novaData[i] = data[numberOfData - i - 1];
   }
   
   dataZaWHO = novaData;
   console.log(dataZaWHO);
   
  });
  
 
}




function pridobiPodatke(callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        console.log(json);
        
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}